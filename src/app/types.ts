export enum WorkerInputType {
  "start",
  "status"
}
export enum WorkerResponseType {
  "complete",
  "status"
}

export interface WorkerInput {
  type: WorkerInputType;
  arg: any;
}

export interface WorkerResponse {
  type: WorkerResponseType;
  data: any;
}
