// interface ThumbnailData {
//   name: string;
//   blobstring: string;
//   type: string;
// }

export interface FileData {
  name?: string;
  thumbSrc?: string;
  mainSrc?: string;
  id: string;
  img?: ImageData;
}
