//!!!!!!!-------DIDNT WORK------!!!!!!!

import * as functions from "firebase-functions";
import { Storage } from "@google-cloud/storage";
import { tmpdir } from "os";
import { join } from "path";
import * as sharp from "sharp";
import * as fs from "fs-extra";
import * as btb from "blob-to-buffer";

interface ThumbnailData {
  name: string;
  blobstring: string;
  type: string;
}

const gcs = new Storage();

export const generateThumbs = functions.https.onCall(
  async (data: ThumbnailData, context) => {
    const bucket = gcs.bucket("test-54717");

    const workingDir = join(tmpdir(), "thumbs");

    // // 1. Ensure thumbnail dir exists
    await fs.ensureDir(workingDir);

    // //2. create file blob
    const fileBlob = new Blob([data.blobstring], {
      type: data.type
    });

    const bufferPromise = new Promise<Buffer>((res, rej) => {
      btb(fileBlob, (err, buffer) => {
        if (err) {
          rej(err);
        } else {
          res(buffer);
        }
      });
    });

    const fileBuffer = await bufferPromise;

    // // 3. Resize the images and define an array of upload promises
    const sizes = [64, 128, 256];

    const uploadPromises = sizes.map(async size => {
      const thumbName = `thumb@${size}_${data.name}`;
      const thumbPath = join(workingDir, thumbName);

      // Resize source image
      await sharp(fileBuffer)
        .resize(size, size)
        .toFile(thumbPath);

      // Upload to GCS
      return bucket.upload(
        thumbPath
        //     , {
        //    destination: join(bucketDir, thumbName)
        //  }
      );
    });

    // // 4. Run the upload operations
    await Promise.all(uploadPromises);

    // // 5. Cleanup remove the tmp/thumbs from the filesystem
    return fs.remove(workingDir);
  }
);
