import {
  WorkerInput,
  WorkerInputType,
  WorkerResponseType,
  WorkerResponse
} from "../types";

const sendMessage: any = self.postMessage;

class ResizeImageWorker {
  currNum = 0;
  sendStatus() {
    sendMessage({
      type: WorkerResponseType.status,
      data: { result: this.currNum }
    } as WorkerResponse);
  }

  calculate(loops: number) {
    this.currNum = 0;

    for (let i = 0; i < loops; i++) {
      this.currNum++;
      if (this.currNum % 10000 === 0) {
        this.sendStatus();
      }
    }
    return this.currNum;
  }
}

const worker = new ResizeImageWorker();

self.addEventListener("message", message => {
  let eventData = <WorkerInput>message.data;
  switch (eventData.type) {
    case WorkerInputType.start:
      const result = worker.calculate(eventData.arg.loops);

      sendMessage({
        type: WorkerResponseType.complete,
        data: {
          result
        }
      } as WorkerResponse);
      break;
  }
});

if (typeof Worker !== 'undefined') {
  // Create a new
  const worker = new Worker('./resize.worker', { type: 'module' });
  worker.onmessage = ({ data }) => {
    console.log(`page got message: ${data}`);
  };
  worker.postMessage('hello');
} else {
  // Web Workers are not supported in this environment.
  // You should add a fallback so that your program still executes correctly.
}