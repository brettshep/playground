import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { RouterModule, Routes } from "@angular/router";
import { ThumbnailsComponent } from "./thumbnails.component";

export const ROUTES: Routes = [
  {
    path: "",
    component: ThumbnailsComponent
  },
  { path: "**", redirectTo: "/thumbnails" }
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(ROUTES)],
  declarations: [ThumbnailsComponent]
})
export class ThumbnailsModule {}
