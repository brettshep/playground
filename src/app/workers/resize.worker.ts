/// <reference lib="webworker" />

import {
  WorkerInput,
  WorkerInputType,
  WorkerResponseType,
  WorkerResponse
} from "../types";

const sendMessage: any = postMessage;

class ResizeImage {
  rust;
  // sendStatus() {
  //   sendMessage({
  //     type: WorkerResponseType.status,
  //     data: { result: this.currNum }
  //   } as WorkerResponse);
  // }

  resizeOld(
    sourceImageData: Uint8Array,
    blankData: Uint8Array,
    sourceW: number,
    sourceH: number,
    width: number,
    height: number
  ) {
    const ratio_w = sourceW / width;
    const ratio_h = sourceH / height;
    const ratio_w_half = Math.ceil(ratio_w / 2);
    const ratio_h_half = Math.ceil(ratio_h / 2);
    //create pixel data
    let weight = 0;
    let weights = 0;
    let weights_alpha = 0;
    let gx_r = 0;
    let gx_g = 0;
    let gx_b = 0;
    let gx_a = 0;
    let x2 = 0;
    let center_y = 0;
    let yy_start = 0;
    let yy_stop = 0;
    let dy = 0;
    let center_x = 0;
    let w0 = 0;
    let xx_start = 0;
    let xx_stop = 0;
    let dx = 0;
    let w = 0;
    let pos_x = 0;
    let j = 0;
    let i = 0;
    let yy = 0;
    let xx = 0;
    let trans_index = blankData.length - 1;
    for (j = 0; j < height; j++) {
      for (i = 0; i < width; i++) {
        x2 = (i + j * width) * 4;
        center_y = (j + 0.5) * ratio_h;
        yy_start = Math.floor(j * ratio_h);
        yy_stop = Math.ceil((j + 1) * ratio_h);
        weight = 0;
        weights = 0;
        weights_alpha = 0;
        gx_r = 0;
        gx_g = 0;
        gx_b = 0;
        gx_a = 0;
        for (yy = yy_start; yy < yy_stop; yy++) {
          dy = Math.abs(center_y - (yy + 0.5)) / ratio_h_half;
          center_x = (i + 0.5) * ratio_w;
          w0 = dy * dy; //pre-calc part of w
          xx_start = Math.floor(i * ratio_w);
          xx_stop = Math.ceil((i + 1) * ratio_w);
          if (xx_stop > sourceW) {
            xx_stop = sourceW;
          }
          for (xx = xx_start; xx < xx_stop; xx++) {
            dx = Math.abs(center_x - (xx + 0.5)) / ratio_w_half;
            w = Math.sqrt(w0 + dx * dx);
            if (w >= 1) {
              //pixel too far
              continue;
            }
            //hermite filter
            weight = 2 * w * w * w - 3 * w * w + 1;
            pos_x = 4 * (xx + yy * sourceW);
            //alpha
            gx_a += weight * sourceImageData[pos_x + 3];
            weights_alpha += weight;
            //colors
            if (sourceImageData[pos_x + 3] < 255)
              weight = (weight * sourceImageData[pos_x + 3]) / 250;
            gx_r += weight * sourceImageData[pos_x];
            gx_g += weight * sourceImageData[pos_x + 1];
            gx_b += weight * sourceImageData[pos_x + 2];
            weights += weight;
          }
        }
        blankData[x2] = gx_r / weights;
        blankData[x2 + 1] = Math.round(gx_g / weights);
        blankData[x2 + 2] = Math.round(gx_b / weights);
        blankData[x2 + 3] = Math.round(gx_a / weights_alpha);
        //check for trans
        if (blankData[x2 + 3] < 255) {
          blankData[trans_index] = 1;
        }
      }
    }
    return blankData;
  }

  resizeWasm(
    sourceImageData: Uint8Array,
    blankData: Uint8Array,
    sourceW: number,
    sourceH: number,
    width: number,
    height: number
  ) {
    const data = this.rust.scale_img(
      sourceImageData,
      blankData,
      sourceW,
      sourceH,
      width,
      height
    );

    return data;
  }
}

const resizeImage = new ResizeImage();

import("@brettshepherd/hello-wasm").then(val => {
  resizeImage.rust = val;
});

addEventListener("message", message => {
  let eventData = <WorkerInput>message.data;
  switch (eventData.type) {
    case WorkerInputType.start:
      const args = eventData.arg;
      const result = resizeImage.resizeWasm(
        args.sourceArr,
        args.resizeArr,
        args.source_w,
        args.source_h,
        args.resize_w,
        args.resize_h
      );

      sendMessage({
        type: WorkerResponseType.complete,
        data: { id: args.id, result }
      } as WorkerResponse);
      break;
  }
});
