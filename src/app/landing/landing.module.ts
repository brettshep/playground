import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule, Routes } from "@angular/router";
import { LandingComponent } from "./landing.component";

const ROUTES: Routes = [
  {
    path: "",
    component: LandingComponent
  },
  { path: "**", redirectTo: "/landing" }
];

@NgModule({
  declarations: [LandingComponent],
  imports: [CommonModule, RouterModule.forChild(ROUTES)]
})
export class LandingModule {}
