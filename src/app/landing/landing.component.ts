import { Component, OnInit } from "@angular/core";
import { html } from "common-tags";

import Prism from "prismjs";

@Component({
  selector: "app-landing",
  templateUrl: "./landing.component.html",
  styleUrls: ["./landing.component.sass"]
})
export class LandingComponent {
  constructor() {}

  animationTime = 20;

  // prettier-ignore
  codes = [
`&lt;section class="hero">
    &lt;img src="images/hero.jpg" alt="hands" />
    &lt;h1>Nature is Great So Buy Today.&lt;/h1>
    &lt;button>Buy Now&lt;/button>
&lt;/section>`,
`&lt;section class="detail">
    &lt;div class="detail_block" id="tree_container">
      &lt;div class="detail_block_max_width">
        &lt;img src="images/tree.png" alt="tree" />
        &lt;/div>
      &lt;/div>
    &lt;/div>
    
    &lt;div class="detail_block">
      &lt;div class="detail_block_max_width">
        &lt;h1>Trees are great. 3D trees are even better.&lt;/h1>
        &lt;p>
        Trees make oxygen which is a pretty good thing to have. I mean, just
        look at the leaves, so green! We offer the best trees available.
        &lt;/p>
      &lt;/div>
    &lt;/div>
  &lt;/section>`, 
`&lt;header>
  &lt;!-- Logo -->
  &lt;div class="logo">&lt;img src="images/logo.png" alt="logo" />&lt;/div>
  
  &lt;!-- Example Links -->
  &lt;nav class="desktop_nav">
    &lt;a>HOME&lt;/a>
    &lt;a>CONTACT&lt;/a>
    &lt;a>ABOUT&lt;/a>
  &lt;/nav>

  &lt;div class="mobile_menu_btn">
    &lt;span>&lt;/span>
    &lt;span>&lt;/span>
    &lt;span>&lt;/span>
  &lt;/div>
&lt;/header>`];

  pages = [
    "landing.png",
    "landing2.png",
    "landing3.png",
    "landing4.png",
    "landing5.png",
    "landing6.png",
    "landing7.png",
    "landing8.png"
  ];

  ngAfterViewInit() {
    Prism.highlightAll();
  }

  copy(e: Event, p: HTMLParagraphElement) {
    // I Element
    const i = e.target as HTMLElement;
    // get pre tag
    const target = i.closest(".preCont").querySelector("pre") as HTMLPreElement;

    const text = target.textContent;
    // Copy Text
    //create elem and copy
    let textArea = document.createElement("textarea");
    textArea.value = text;
    document.body.appendChild(textArea);
    textArea.style.position = "fixed";
    textArea.style.background = "transparent";
    textArea.style.top = "0";
    textArea.style.left = "0";

    if (this.isIOS) {
      textArea.contentEditable = "true";
      textArea.readOnly = true;

      // create a selectable range
      var range = document.createRange();
      range.selectNodeContents(textArea);

      // select the range
      var selection = window.getSelection();
      selection.removeAllRanges();
      selection.addRange(range);
      textArea.setSelectionRange(0, 999999);
    } else {
      textArea.focus();
      textArea.select();
    }

    document.execCommand("copy");
    document.body.removeChild(textArea);

    // Toggle Copy Text
    p.classList.add("active");
    setTimeout(() => {
      p.classList.remove("active");
    }, 1000);
  }

  get isIOS(): boolean {
    return !!navigator.userAgent.match(/ipad|ipod|iphone/i);
  }
}
