import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { ThumbnailService } from "./thumbnail.service";
import { FileData } from "../../../interfaces";

@Component({
  selector: "thumbnails",
  template: `
    <div class="wrapper">
      <input type="file" (change)="onFileChange($event)" multiple />
      <div class="grid">
        <!-- file Data -->
        <div *ngFor="let fileData of fileDataArr" class="gridItem">
          <!-- image container -->
          <div *ngIf="fileData.thumbSrc; else loading" class="imgCont">
            <div class="filterWrap">
              <img [src]="fileData.thumbSrc" alt="image" />
            </div>
            <div class="filterWrap stack1">
              <img [src]="fileData.thumbSrc" alt="image" />
            </div>
            <div class="filterWrap stack2">
              <img [src]="fileData.thumbSrc" alt="image" />
            </div>
          </div>
          <!-- file data -->
          <h3>{{ fileData.name }}</h3>
          <h5><span></span>Inital Draft</h5>
        </div>
      </div>
    </div>
    <!-- Loading Spinner-->
    <ng-template #loading>
      <div class="imgCont imgContLoading">
        <div class="spinner"></div>
      </div>
    </ng-template>
  `,
  styleUrls: ["./thumbnails.component.sass"]
})
export class ThumbnailsComponent implements OnInit {
  canvas: HTMLCanvasElement;
  ctx: CanvasRenderingContext2D;
  thumbMaxSize: { width: number; height: number } = {
    width: 400,
    height: 300
  };
  mainMaxSize: { width: number; height: number } = { width: 1200, height: 900 };
  fileDataArr: FileData[] = [];
  start;
  end;

  constructor(
    private thumbServ: ThumbnailService,
    private cd: ChangeDetectorRef
  ) {}

  ngOnInit() {
    this.canvas = document.createElement("canvas");
    this.ctx = this.canvas.getContext("2d");

    //subscribe to file computed
    this.thumbServ.fileComputed$.subscribe(val => {
      const file = this.fileDataArr.find(fileData => fileData.id === val.id);

      const src = this.getDataUrlFromArray(val.result, file.img);
      file.thumbSrc = src;

      this.end = performance.now();
      console.log(this.end - this.start);
    });
  }

  onFileChange(e) {
    this.start = performance.now();
    const files: File[] = e.target.files;

    for (let i = 0; i < files.length; i++) {
      const file = files[i];
      const imageType = /image.*/;
      if (file.type.match(imageType)) {
        const id = this.thumbServ.randID;
        let fileData: FileData = { id };
        //add fileData to array
        this.fileDataArr = [...this.fileDataArr, fileData];
      }
    }

    setTimeout(() => {
      //for each file
      for (let i = 0; i < this.fileDataArr.length; i++) {
        //see if file
        const file = files[i];
        const imageType = /image.*/;

        //see if image file
        if (file.type.match(imageType)) {
          const dataUrl = URL.createObjectURL(file);
          const img = new Image();

          //gen file id
          let fileData: FileData = this.fileDataArr[i];

          img.onload = () => {
            //-----ORIGINAL SIZE--------

            //set canvas width and height
            this.canvas.width = img.width;
            this.canvas.height = img.height;

            //draw original image into canvas
            this.ctx.drawImage(img, 0, 0);

            //get image data
            const sourceImageData = this.ctx.getImageData(
              0,
              0,
              img.width,
              img.height
            );

            const sourceImgDataArr = new Uint8Array(sourceImageData.data);

            //-------RESIZED---------
            const dimensions = this.calculateResizeDimensions(
              img.width,
              img.height,
              this.thumbMaxSize.width,
              this.thumbMaxSize.height
            );

            //get file name
            const nameArr = file.name.match(/(.*)\.(.*?)$/);
            const name = nameArr[1];
            fileData.name = name;

            if (!dimensions) {
              const src = this.compressNonResampledImage(sourceImgDataArr);
              fileData.thumbSrc = src;
              this.fileDataArr = [...this.fileDataArr, fileData];
              return;
            }
            //round downsized resolution
            dimensions.resizedWidth = Math.round(dimensions.resizedWidth);
            dimensions.resizedHeight = Math.round(dimensions.resizedHeight);

            const resizeImgData = this.ctx.createImageData(
              dimensions.resizedWidth,
              dimensions.resizedHeight
            );

            //set initial file data

            fileData.img = resizeImgData;

            //calculate mainSrc
            // this.setMainImageSrc(id, imgData, width, height, fileData);

            // //reset canvas width and height
            // this.canvas.width = img.width;
            // this.canvas.height = img.height;

            //calculate thumbsrc
            this.resizeImage(
              fileData.id,
              resizeImgData,
              sourceImgDataArr,
              img.width,
              img.height,
              dimensions.resizedWidth,
              dimensions.resizedHeight
            );
            //calculate thumbSrc
          };

          img.src = dataUrl;
        }
      }
    }, 0);
  }

  // setMainImageSrc(
  //   id: string,
  //   imgData: Uint8Array,
  //   sourceW: number,
  //   sourceH: number,
  //   fileData: FileData
  // ) {
  //   const dimensions = this.calculateResizeDimensions(
  //     sourceW,
  //     sourceH,
  //     this.mainMaxSize.width,
  //     this.mainMaxSize.height
  //   );

  //   //if no dimensions, image doesnt need resize, check for compression
  //   if (!dimensions) {
  //     const src = this.compressNonResampledImage(imgData);
  //     fileData.mainSrc = src;
  //     return;
  //   }

  //   // resize image
  //   const src = this.resizeImage(
  //     id,
  //     imgData,
  //     sourceW,
  //     sourceH,
  //     dimensions.resizedWidth,
  //     dimensions.resizedHeight
  //   );
  //   fileData.mainSrc = src;
  // }

  // setThumbImageSrc(
  //   id: string,
  //   imgData: Uint8Array,
  //   sourceW: number,
  //   sourceH: number,
  //   fileData: FileData
  // ) {

  //   //if no dimensions, image doesnt need resize, just use mainSrc
  //   if (!dimensions) {
  //     fileData.thumbSrc = fileData.mainSrc;
  //     return;
  //   }

  //   // resize image
  //   const src = this.resizeImage(
  //     id,
  //     imgData,
  //     sourceW,
  //     sourceH,
  //     dimensions.resizedWidth,
  //     dimensions.resizedHeight
  //   );
  //   fileData.thumbSrc = src;
  // }

  calculateResizeDimensions(
    sourceW: number,
    sourceH: number,
    width: number,
    height: number
  ) {
    //see if image needs to be resized
    if (sourceW <= width && sourceH <= height) {
      //if no resize, image is small enough to use original mainSrc
      return null;
    }

    const ratio = Math.min(width / sourceW, height / sourceH);

    const resizedWidth = sourceW * ratio;
    const resizedHeight = sourceH * ratio;

    return { resizedWidth, resizedHeight };
  }

  resizeImage(
    id: string,
    resizeImgData: ImageData,
    sourceImageData: Uint8Array,
    sourceW: number,
    sourceH: number,
    width: number,
    height: number
  ): string {
    //get image data
    const blankData = new Uint8Array([...resizeImgData.data, 0]);

    this.thumbServ.workerResizeImage(
      id,
      sourceImageData,
      blankData,
      sourceW,
      sourceH,
      width,
      height
    );
    // console.log(sourceImageData, blankData, sourceW, sourceH, width, height);

    // const resized = this.thumbServ.resizeImage(
    //   sourceImageData,
    //   blankData,
    //   sourceW,
    //   sourceH,
    //   width,
    //   height
    // );

    // const hasTrans = !!resized[resized.length - 1];

    // //set image data
    // resizeImgData.data.set(resized.slice(0, resized.length - 1));

    // this.canvas.width = width;
    // this.canvas.height = height;

    // this.ctx.putImageData(resizeImgData, 0, 0);

    // if (hasTrans) {
    //   return this.canvas.toDataURL();
    // }
    // //no transparency, can compress
    // else {
    //   return this.canvas.toDataURL("image/jpeg", 0.9);
    // }

    return "";

    // const hasTrans = !!resized[resized.length - 1];

    // //set image data
    // img2.data.set(resized.slice(0, resized.length - 1));

    // //resize canvas
    // this.canvas.width = width;
    // this.canvas.height = height;

    // //draw new img data to canvas
    // this.ctx.putImageData(img2, 0, 0);

    // //--return data as url--

    // //has transparency, cant compress
    // if (hasTrans) {
    //   return this.canvas.toDataURL();
    // }
    // //no transparency, can compress
    // else {
    //   return this.canvas.toDataURL("image/jpeg", 0.9);
    // }
  }

  getDataUrlFromArray(resized: Uint8Array, img: ImageData) {
    const hasTrans = !!resized[resized.length - 1];

    //set image data
    img.data.set(resized.slice(0, resized.length - 1));

    //resize canvas
    this.canvas.width = img.width;
    this.canvas.height = img.height;

    //draw new img data to canvas
    this.ctx.putImageData(img, 0, 0);

    //--return data as url--

    //has transparency, cant compress
    if (hasTrans) {
      return this.canvas.toDataURL();
    }
    //no transparency, can compress
    else {
      return this.canvas.toDataURL("image/jpeg", 0.9);
    }
  }

  compressNonResampledImage(sourceImgData: Uint8Array): string {
    let hasTrans = false;

    for (let i = 3, n = sourceImgData.length; i < n; i += 4) {
      if (sourceImgData[i] < 255) {
        hasTrans = true;
        break;
      }
    }

    if (hasTrans) {
      return this.canvas.toDataURL();
    }
    //no transparency, can compress
    else {
      return this.canvas.toDataURL("image/jpeg", 0.9);
    }
  }

  tempCheckTrans(sourceImgData: Uint8Array): boolean {
    for (let i = 3, n = sourceImgData.length; i < n; i += 4) {
      if (sourceImgData[i] < 255) {
        return true;
        break;
      }
    }
    return false;
  }
}

// const ratio_w = sourceW / width;
// const ratio_h = sourceH / height;
// const ratio_w_half = Math.ceil(ratio_w / 2);
// const ratio_h_half = Math.ceil(ratio_h / 2);

// //create pixel data

// let weight = 0;
// let weights = 0;
// let weights_alpha = 0;
// let gx_r = 0;
// let gx_g = 0;
// let gx_b = 0;
// let gx_a = 0;
// let x2 = 0;
// let center_y = 0;
// let yy_start = 0;
// let yy_stop = 0;
// let dy = 0;
// let center_x = 0;
// let w0 = 0;
// let xx_start = 0;
// let xx_stop = 0;
// let dx = 0;
// let w = 0;
// let pos_x = 0;
// let j = 0;
// let i = 0;
// let yy = 0;
// let xx = 0;
// let trans_index = blankData.length - 1;

// for (j = 0; j < height; j++) {
//   for (i = 0; i < width; i++) {
//     x2 = (i + j * width) * 4;
//     center_y = (j + 0.5) * ratio_h;
//     yy_start = Math.floor(j * ratio_h);
//     yy_stop = Math.ceil((j + 1) * ratio_h);
//     weight = 0;
//     weights = 0;
//     weights_alpha = 0;
//     gx_r = 0;
//     gx_g = 0;
//     gx_b = 0;
//     gx_a = 0;
//     for (yy = yy_start; yy < yy_stop; yy++) {
//       dy = Math.abs(center_y - (yy + 0.5)) / ratio_h_half;
//       center_x = (i + 0.5) * ratio_w;
//       w0 = dy * dy; //pre-calc part of w
//       xx_start = Math.floor(i * ratio_w);
//       xx_stop = Math.ceil((i + 1) * ratio_w);

//       if (xx_stop > sourceW) {
//         xx_stop = sourceW;
//       }

//       for (xx = xx_start; xx < xx_stop; xx++) {
//         dx = Math.abs(center_x - (xx + 0.5)) / ratio_w_half;
//         w = Math.sqrt(w0 + dx * dx);

//         if (w >= 1) {
//           //pixel too far
//           continue;
//         }
//         //hermite filter
//         weight = 2 * w * w * w - 3 * w * w + 1;
//         pos_x = 4 * (xx + yy * sourceW);

//         //alpha
//         gx_a += weight * sourceImageData[pos_x + 3];
//         weights_alpha += weight;
//         //colors
//         if (sourceImageData[pos_x + 3] < 255)
//           weight = (weight * sourceImageData[pos_x + 3]) / 250;
//         gx_r += weight * sourceImageData[pos_x];
//         gx_g += weight * sourceImageData[pos_x + 1];
//         gx_b += weight * sourceImageData[pos_x + 2];
//         weights += weight;
//       }
//     }
//     blankData[x2] = gx_r / weights;
//     blankData[x2 + 1] = Math.round(gx_g / weights);
//     blankData[x2 + 2] = Math.round(gx_b / weights);
//     blankData[x2 + 3] = Math.round(gx_a / weights_alpha);
//     //check for trans
//     if (blankData[x2 + 3] < 255) {
//       blankData[trans_index] = 1;
//     }
//   }
// }
// const hasTrans = !!blankData[blankData.length - 1];
// console.log("HasTrans: ", hasTrans);
// img2.data.set(blankData.slice(0, blankData.length - 1));
