import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
// import { HttpClientModule, HttpClientJsonpModule } from "@angular/common/http";
import { AppRoutingModule } from "./routing.module";
import { AppComponent } from "./app.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { Store } from "./store";

// Firebase
import { AngularFireModule } from "@angular/fire";
import { AngularFireStorageModule } from "@angular/fire/storage";
import { AngularFirestoreModule } from "@angular/fire/firestore";
import { AngularFireFunctionsModule } from "@angular/fire/functions";

const config = {
  apiKey: "AIzaSyD4bV6_0CNLTHBn3ANbTo-RgaZuHtZJQeE",
  authDomain: "test-54717.firebaseapp.com",
  databaseURL: "https://test-54717.firebaseio.com",
  projectId: "test-54717",
  storageBucket: "test-54717.appspot.com",
  messagingSenderId: "519684832018",
  appId: "1:519684832018:web:14a0dbf1093980422551cb",
  measurementId: "G-MWK5YQB6VG"
};

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    // HttpClientModule,
    // HttpClientJsonpModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(config),
    AngularFirestoreModule,
    AngularFireStorageModule,
    AngularFireFunctionsModule
  ],
  providers: [Store],
  bootstrap: [AppComponent]
})
export class AppModule {}
