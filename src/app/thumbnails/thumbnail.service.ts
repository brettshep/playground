import { Injectable } from "@angular/core";
// import * as rust from "@brettshepherd/hello-wasm";

import {
  WorkerResponse,
  WorkerResponseType,
  WorkerInputType,
  WorkerInput
} from "../types";

import { Subject } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class ThumbnailService {
  // public fibonacci(input: number) {
  //   const arr = new Uint8Array(100) as Uint8Array;
  //   console.log(rust.num_arr(arr));
  // }
  fileComputed$: Subject<any> = new Subject();
  resizeWorker: Worker;

  constructor() {
    this.resizeWorker = new Worker(`../workers/resize.worker`, {
      type: `module`
    });

    this.resizeWorker.addEventListener("message", (message: any) => {
      const responseData = <WorkerResponse>message.data;

      switch (responseData.type) {
        case WorkerResponseType.status:
          console.log(responseData.data);
          break;
        case WorkerResponseType.complete:
          this.fileComputed$.next(responseData.data);
          break;
      }
    });
  }

  // workerTest(num: number) {
  //   this.resizeImageWorker.postMessage({
  //     type: WorkerInputType.start,
  //     arg: {
  //       loops: num
  //     }
  //   } as WorkerInput);
  // }

  workerResizeImage(
    id: string,
    sourceArr: Uint8Array,
    resizeArr: Uint8Array,
    source_w: number,
    source_h: number,
    resize_w: number,
    resize_h: number
  ) {
    const input: WorkerInput = {
      type: WorkerInputType.start,
      arg: {
        id,
        sourceArr,
        resizeArr,
        source_w,
        source_h,
        resize_w,
        resize_h
      }
    };
    this.resizeWorker.postMessage(input);
  }

  // resizeImage(
  //   sourceArr: Uint8Array,
  //   resizeArr: Uint8Array,
  //   source_w: number,
  //   source_h: number,
  //   resize_w: number,
  //   resize_h: number
  // ) {
  //   return rust.scale_img(
  //     sourceArr,
  //     resizeArr,
  //     source_w,
  //     source_h,
  //     resize_w,
  //     resize_h
  //   );
  // }

  get randID() {
    return (
      "_" +
      Math.random()
        .toString(36)
        .substr(2, 9)
    );
  }
}
