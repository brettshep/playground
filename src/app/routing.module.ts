import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

export enum CompName {
  thumbnails = 1,
  landing
}

const routes: Routes = [
  {
    path: "thumbnails",
    data: { name: CompName.thumbnails },
    loadChildren: () =>
      import("./thumbnails/thumbnails.module").then(m => m.ThumbnailsModule)
  },
  {
    path: "landing",
    data: { name: CompName.thumbnails },
    loadChildren: () =>
      import("./landing/landing.module").then(m => m.LandingModule)
  },
  { path: "**", redirectTo: "landing", pathMatch: "full" }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
